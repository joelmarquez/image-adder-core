package com.etermax

import java.io.File

import com.etermax.files.OptionS
import org.apache.commons.cli._
import org.apache.commons.io.FileUtils

import scala.collection.mutable.ArrayBuffer

import scala.collection.JavaConverters._

object ImageAdder extends App {

  def doCopyFiles(originFiles: Map[String, File], destinationFiles: Map[String, File], extension: String, charsToFilter: (String, String) = (" ", " ")): List[String] = {

    val notAdded = ArrayBuffer.empty[String]

    val filteredFiles = originFiles.filter (p => p._1.endsWith(extension) && !p._1.contains(charsToFilter._1) && !p._1.contains(charsToFilter._2))
    filteredFiles.foreach { orig =>

      if (destinationFiles.keySet.contains(orig._1)) {
        val srcFile = orig._2
        val destFile = destinationFiles(orig._1)

        FileUtils.copyFile(srcFile, destFile)
      }
      else {
        notAdded += orig._2.getCanonicalPath
      }
    }

    notAdded.toList
  }

  def copyFiles(originFiles: Map[String, File], destinationFiles: Map[String, File], copies: (Boolean, Boolean, Boolean, Boolean, Boolean)): String = {

    val notAdded = ArrayBuffer.empty[String]

    if (copies._4) {
      if (copies._1) {
        notAdded ++= doCopyFiles(originFiles, destinationFiles, ".png", ("@", "~"))
      }
      if (copies._2) {
        notAdded ++= doCopyFiles(originFiles, destinationFiles, "@2x.png")
      }
      if (copies._3) {
        notAdded ++= doCopyFiles(originFiles, destinationFiles, "@3x.png")
      }
    }
    if (copies._5) {
      if (copies._1) {
        notAdded ++= doCopyFiles(originFiles, destinationFiles, "~ipad.png", ("@", " "))
      }
      if (copies._2) {
        notAdded ++= doCopyFiles(originFiles, destinationFiles, "@2x~ipad.png")
      }
    }

    notAdded.toList.sorted.mkString("\n")
  }

  def buildOptions:Options = {

    val options = new Options

    val sourceDirOpt = OptionS("od", "origin directory where the design images are").asOption
    val destDirOpt = OptionS("dd", "destination directory where the design images are going to be copied").asOption

    val x1Opt = OptionS("1", "copy 1x images", Some(false), Some(false)).asOption
    val x2Opt = OptionS("2", "copy 2x images", Some(false), Some(false)).asOption
    val x3Opt = OptionS("3", "copy 3x images", Some(false), Some(false)).asOption
    val iPhoneOpt = OptionS("h", "copy iPhone images", Some(false), Some(false)).asOption
    val iPadOpt = OptionS("a", "copy iPad images", Some(false), Some(false)).asOption

    options.addOption(sourceDirOpt)
            .addOption(destDirOpt)
            .addOption(x1Opt)
            .addOption(x2Opt)
            .addOption(x3Opt)
            .addOption(iPhoneOpt)
            .addOption(iPadOpt)

    options
  }

  override def main(args: Array[String]): Unit = {

    val options = buildOptions
    val parser = new PosixParser

    var cmd: CommandLine = null
    try {
      cmd = parser.parse(options, args)
    }
    catch {
      case e: ParseException =>
        val formatter = new HelpFormatter
        formatter.printHelp("image-adder", options)
        System.exit(1)
    }

    val originDir = new File(cmd.getOptionValue("od"))
    val destinationDir = new File(cmd.getOptionValue("dd"))

    if (!originDir.exists || !destinationDir.exists) {
      println(s"Error: ${args(0)} or ${args(1)} doesn't extist")
      System.exit(2)
    }

    val originFiles = FileUtils.listFiles(originDir, Array("png"), true).asScala.toList.map(f => f.getName -> f).toMap
    val destinationFiles = FileUtils.listFiles(destinationDir, Array("png"), true).asScala.toList.map(f => f.getName -> f).toMap

    println(s"${copyFiles(originFiles, destinationFiles, (cmd.hasOption('1'), cmd.hasOption('2'), cmd.hasOption('3'), cmd.hasOption('h'), cmd.hasOption('a')))}")
  }
}
