package com.etermax.files

case class OptionS(opt: String,
                       description: String,
                       required: Option[Boolean] = Some(true),
                       hasArg: Option[Boolean] = Some(true),
                       longOpt: Option[String] = Some("")) {

  def asOption: org.apache.commons.cli.Option = {
    val result = new org.apache.commons.cli.Option(opt, description)
    result.setRequired(required.get)
    result.setArgs(if (hasArg.get) 1 else 0)
    result.setLongOpt(longOpt.get)
    result
  }
}
