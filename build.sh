#!/bin/bash

if [ ! $SCALA_HOME ]
then
    echo ERROR: set a SCALA_HOME environment variable
    exit
fi

if [ ! -f scala-library.jar ]
then
    cp $SCALA_HOME/lib/scala-library.jar .
fi

scalac -classpath "*.jar" src/ImageAdder.scala
cd out/production/image-adder
jar -cfm ../../../image-adder.jar ../../../MANIFEST.MF *
cd ../../../

java -jar image-adder.jar
